package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class ListViewActivity extends AppCompatActivity {
    ListView lv;
    TextView tv;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        lv = findViewById(R.id.listView);
        tv = findViewById(R.id.textView);
        linearLayout = findViewById(R.id.linearLayout);
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.list_item, getResources().getStringArray(R.array.day));
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tv.setText(parent.getItemAtPosition(position).toString());
                Toast.makeText(ListViewActivity.this, parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();
                Snackbar.make(linearLayout, "done", Snackbar.LENGTH_INDEFINITE).show();
                showDialog();
            }
        });
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("java aasaan hai");
        builder.setMessage("java sabse aasaan hai");
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ListViewActivity.this, "continue", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
