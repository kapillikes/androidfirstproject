package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        Log.e("method1","onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("method1","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("method1","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("method1","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("method1","onstop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("method1","onDestroy");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("method1","onrestart");


    }
}
