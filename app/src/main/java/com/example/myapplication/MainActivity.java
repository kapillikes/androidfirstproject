package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button=findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        Log.e("method","onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("method","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("method","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("method","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("method","onstop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("method","onDestroy");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("method","onrestart");


    }
}

